# Qantas Hotels Coding Challenge

Applicant: Dave Greig

Below are the instructions provided for the challenge.
Please see the `Notes` section for comments from the developer.

# Instructions

We would like you to work on this coding test so we can see how you would approach building out this feature. We think you should spend about 4 hours on the code test, we consider it a starting point to extend and modify in our technical interview.

## User Story to Implement

As a user of Qantas hotels I would like to see a list of hotels that can be sorted by price.

## More Information

Use the attached image of the search page as a guide and feel free to use any technology that you are comfortable with, it is not neccessary to match fonts and colours, e.g. we are cool if you choose Comic Sans and red and green.

Implement both these sorting options:

- Price (high-low)
- Price (low-high)

Star ratings on properties are divided into two types: ‘star’ and ‘self’ rated. Properties that are ‘self’ rated use a circle icon and properties that are ‘star’ rated use a star icon.

We value writing well tested code at Hooroo and would like to see tests around any code you write.

Feel free to send through any questions you may have.

## Included files

- JSON payload
- Mock of Qantas search results
- Qantas Logo

# Notes:

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

To run the webserver, simply type `yarn start` or `npm start` from the command line (depending on what package manager you prefer).
Starting the app does two things:

- starts a mock API at `http://localhost:3001/`. A GET request to the `/hotels` endpoint results in dummy hotel data to be returned and consumed by the application.
- starts the webserver and opens `http://localhost:3000/` in a browser window.

## Regarding the mock API:

- The API endpoint consumed by the application is configured as `mockApiUrl` in `App.js` - you may change this to any valid REST API provided the payload structure is the same.
- If you simply want to edit the mock payload data, all you need to do is update `input/data.json` with your changes.
- You can also start the mock API independently by running `node scripts/mockApiServer.js` from the command line.

## User experience

Not implemented, but there are opportunites to build a better user experience by displaying a loading spinner while the page awaits the results from the hotel API. When the state attribute `loading` is true, an empty div is displayed.
Similarly, if there are no results returned from the API then a basic message stating as such will be returned to the user.
The application employs no pagination, and this could be problematic if many search results are returned.
There is no loading spinner displayed while image retrieval occurs. Requests to load images happens all at once - perhaps lazy loading could be used if the hotel search result set is large enough.
