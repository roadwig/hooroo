import React from 'react';
import { shallow } from 'enzyme';
import { omit } from 'underscore';
import HotelCard from '../../components/HotelCard';
import HotelImage from '../../components/HotelImage';
import HotelRating from '../../components/HotelRating';

describe('HotelCard', () => {
  let testHotel;
  beforeEach(() => {
    testHotel = {
      id: 'test123',
      title: 'Sunshine Motor Inn',
      address: 'Ballarat Rd, Sunshine',
      image: 'https://unsplash.it/145/125/?random',
      rating: 1,
      ratingType: 'star',
      promotion: 'Going cheap',
      roomName: 'Filthy shoebox',
      price: 50,
      savings: 280,
      freeCancellation: true
    };
  });

  it('renders the HotelImage component with the correct props', () => {
    const hotelCard = shallow(<HotelCard hotel={ testHotel } />);
    expect(hotelCard.find(HotelImage).length).toBe(1);
    expect(hotelCard.find(HotelImage).props()).toEqual({
      image: 'https://unsplash.it/145/125/?random',
      badge: 'Going cheap'
    });
  });

  it('renders the HotelRating component', () => {
    const hotelCard = shallow(<HotelCard hotel={ testHotel } />);
    expect(hotelCard.find(HotelRating).length).toBe(1);
    expect(hotelCard.find(HotelRating).props()).toEqual({
      rating: 1,
      ratingType: 'star'
    });
  });

  it('renders a saving value if there is a price reduction', () => {
    const hotelCard = shallow(<HotelCard hotel={ testHotel } />);
    expect(hotelCard.html().includes('Save $280~')).toBe(true);
  });

  it('does not render a saving value if there is none provided', () => {
    const hotelWithoutSaving = omit(testHotel, 'savings');
    const hotelCard = shallow(<HotelCard hotel={ hotelWithoutSaving } />);
    expect(hotelCard.html().includes('Save $')).toBe(false);
  });

  it('displays the cancellation policy when the hotel allows free cancellation', () => {
    const hotelCard = shallow(<HotelCard hotel={ testHotel } />);
    expect(hotelCard.html().includes('Free Cancellation')).toBe(true);
  });

  it('does not display any cancellation policy when the hotel has not specified free cancellation', () => {
    const hotelWithoutFreeCancellation = testHotel;
    hotelWithoutFreeCancellation.freeCancellation = false;
    const hotelCard = shallow(
        <HotelCard hotel={ hotelWithoutFreeCancellation } />
    );
    expect(hotelCard.html().includes('Free Cancellation')).toBe(false);
  });
});
