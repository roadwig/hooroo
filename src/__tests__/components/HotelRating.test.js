import React from 'react';
import { mount, shallow } from 'enzyme';
import HotelRating from '../../components/HotelRating';
import StarRatingComponent from 'react-star-rating-component';

describe('HotelRating', () => {
  beforeEach(() => {});

  it('wraps the StarRating component', () => {
    const hotelRating = mount(<HotelRating rating={ 5 } ratingType={ 'star' } />);
    expect(hotelRating.find(StarRatingComponent)).toHaveLength(1);
  });
  it('renders a star rating for a hotel', () => {
    const hotelRating = mount(<HotelRating rating={ 5 } ratingType={ 'star' } />);
    expect(hotelRating.children().find('[name="hotelRating"]')).toBeDefined();
    expect(hotelRating.html().includes('<span>★</span>')).toBe(true);
  });

  it('renders a circle rating for a self-rated hotel', () => {
    const hotelRating = mount(<HotelRating rating={ 5 } ratingType={ 'self' } />);
    expect(hotelRating.children().find('[name="hotelRating"]')).toBeDefined();
    expect(hotelRating.html().includes('<span>●</span>')).toBe(true);
  });

  it('renders a rating that cannot be edited', () => {
    const hotelRating = mount(<HotelRating rating={ 5 } ratingType={ 'star' } />);

    expect(hotelRating.find(StarRatingComponent).props().editing).toBe(false);
  });

  it('renders a rating where the maximum value is 5', () => {
    const hotelRating = mount(<HotelRating rating={ 2 } ratingType={ 'star' } />);
    expect(hotelRating.find(StarRatingComponent).props().starCount).toBe(5);
  });
});
