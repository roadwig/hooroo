import React from 'react';
import { shallow } from 'enzyme';
import HotelImage from '../../components/HotelImage';

describe('HotelImage', () => {
  it('renders an image', () => {
    const hotelImageWithBadge = shallow(<HotelImage image="someImage.jpg" />);
    expect(
      hotelImageWithBadge.html().includes('<img src="someImage.jpg"')
    ).toBe(true);
  });

  it('badges an image with a label when supplied', () => {
    const hotelImageWithBadge = shallow(
        <HotelImage image="someImage.jpg" badge="Awesome Deal" />
    );
    expect(
      hotelImageWithBadge.html().includes('<img src="someImage.jpg"')
    ).toBe(true);
    expect(hotelImageWithBadge.html().includes('Awesome Deal')).toBe(true);
  });
});
