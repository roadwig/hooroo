import getHotelData from '../../utils/api';

describe('getHotelData', () => {
  const mockHttp500ResponseStatus = new Error(JSON.stringify({ status: 500 }));
  let mockHotelResponse;

  beforeEach(() => {
    fetch.resetMocks();
  });

  it('should throw an error when the the API fails to respond', async () => {
    expect.assertions(1);
    fetch.mockReject(mockHttp500ResponseStatus);
    await expect(getHotelData()).rejects.toThrow();
  });

  it('should return a count of all hotels, and the hotel objects', async () => {
    expect.assertions(1);
    mockHotelResponse = [
      { id: 'aHotel' },
      { id: 'anotherHotel' },
      { id: 'oneMoreHotel' }
    ];
    const expectedResult = {
      results: 3,
      hotelResults: [
        { id: 'aHotel' },
        { id: 'anotherHotel' },
        { id: 'oneMoreHotel' }
      ]
    };
    fetch.mockResponse(JSON.stringify(mockHotelResponse));
    await expect(getHotelData()).resolves.toEqual(expectedResult);
  });
});
