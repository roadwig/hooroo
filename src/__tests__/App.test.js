import React from 'react';
import renderer from 'react-test-renderer';
import App from '../App';
import * as API from '../utils/api';
import { mount } from 'enzyme';
import HotelCard from '../components/HotelCard';
import { __awaiter } from 'tslib';

const mockHotelDataState = {
  resultCount: 2,
  loading: false,
  hotels: [
    {
      id: '123',
      title: 'A nice hotel',
      address: '10 City Rd, Melbourne',
      image: 'https://example.com/image.jpg',
      rating: 5,
      ratingType: 'self',
      promotion: 'Exclusive Deal',
      roomName: 'Deluxe King',
      price: 900,
      savings: 80,
      freeCancellation: true
    },
    {
      id: '456',
      title: 'Nice Views Melbourne',
      address: '100 WIlliam St, Melbourne',
      image: 'https://example.com/image.png',
      rating: 5,
      ratingType: 'star',
      promotion: 'Red Hot Deal',
      roomName: 'Deluxe',
      price: 300,
      freeCancellation: false
    }
  ]
};

describe('snapshot testing', () => {
  it('renders without crashing', () => {
    const app = renderer.create(<App />).toJSON();
    expect(app).toMatchSnapshot();
  });
});

describe('search results', () => {
  beforeEach(() => {
    // fetch.resetMocks();
  });

  it('renders a hotel card for every hotel object', async () => {
    const hotelSearchResults = mount(<App />);
    hotelSearchResults.instance().setState(mockHotelDataState);
    hotelSearchResults.update();
    await expect(hotelSearchResults.find(HotelCard).length).toBe(2);
  });

  it('renders a message when no hotels are returned', async () => {
    const hotelSearchResults = mount(<App />);
    hotelSearchResults.instance().setState({ loading: false, results: 0 });
    await expect(hotelSearchResults.find(HotelCard).length).toBe(0);
    await expect(
      hotelSearchResults.html().includes("Sorry, we didn't find any results")
    ).toBe(true);
  });
});
