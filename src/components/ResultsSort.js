import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ResultsLabelStyling = styled.div`
  display: block;
  text-align: right;
`;

class ResultsSort extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  static propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    onSortChange: PropTypes.func
  };

  static defaultProps = {
    label: '',
    value: 'price-asc'
  };

  handleChange(event) {
    this.props.handlePriceSorting(event.target.value);
  }

  render() {
    return (
        <div>
            <ResultsLabelStyling>
                <b>Sort by </b>{' '}
                <select value={ this.props.value } onChange={ this.handleChange }>
                    <option value="price-desc">Price high-low</option>
                    <option value="price-asc">Price low-high</option>
                </select>
            </ResultsLabelStyling>
        </div>
    );
  }
}

export default ResultsSort;
