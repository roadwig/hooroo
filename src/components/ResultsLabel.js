import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ResultsLabelStyling = styled.div`
  display: block;
  text-align: left;
`;

class ResultsLabel extends Component {
  static propTypes = {
    results: PropTypes.number.isRequired,
    suburb: PropTypes.string.isRequired
  };

  static defaultProps = {
    results: 0,
    suburb: ''
  };
  render() {
    return (
        <div>
            <ResultsLabelStyling>
                <b>{this.props.results}</b>
                <em> results in </em>
                <b>{this.props.suburb}</b>
            </ResultsLabelStyling>
        </div>
    );
  }
}

export default ResultsLabel;
