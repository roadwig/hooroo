import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ImageStyling = styled.img`
  margin: 0 0;
  right: 0px;
  display: block;
  position: relative;
  z-index: -1;
  width: 100%;
  height: 100%;
`;

const ImageBadgeStyling = styled.div`
  // border-top: 25px solid white;
  border: 1px solid grey;
  background-color: white;
  position: absolute;
  top: 20px;
  padding: 0 5px;
  width: 100px;
  height: 25px;
  color: red;
  font-family: sans-serif;
  font-size: 11px;
`;

const BadgeLabel = styled.div`
  position: absolute;
  // top: -20px;
  // left: 20px;
  top: 5px;
`;

class HotelImage extends Component {
  static propTypes = {
    image: PropTypes.string.isRequired,
    badge: PropTypes.string.isRequired
  };

  static defaultProps = {
    image: '',
    badge: ''
  };

  hasBadge = () => {
    return this.props.badge ? (
        <ImageBadgeStyling>
            <BadgeLabel>{this.props.badge}</BadgeLabel>
        </ImageBadgeStyling>
    ) : (
        <div />
    );
  };
  render() {
    return (
      <>
          {this.hasBadge()}
          <ImageStyling src={ this.props.image } />
      </>
    );
  }
}

export default HotelImage;
