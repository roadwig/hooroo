import React, { Component } from 'react';
import logoImage from '../assets/qantas-logo.png';
import styled from 'styled-components';

const LogoStyling = styled.img`
  max-width: 100%;
  height: auto;
  margin: 0 0;
`;

class Logo extends Component {
  render() {
    return <LogoStyling src={ logoImage } />;
  }
}

export default Logo;
