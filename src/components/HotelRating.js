import React, { Component } from 'react';
import StarRatingComponent from 'react-star-rating-component';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const HotelRatingStyling = styled.div`
  padding: 0px 5px;
  display: inline-block;
  vertical-align: super;
  font-size: 25px;
`;
class HotelRating extends Component {
  static propTypes = {
    rating: PropTypes.number.isRequired,
    ratingType: PropTypes.string.isRequired
  };

  static defaultProps = {
    rating: 0,
    ratingType: ''
  };

  getRatingStyleIcon = () => {
    return this.props.ratingType === 'self' ? <span>●</span> : <span>★</span>;
  };

  render() {
    return (
        <HotelRatingStyling>
            <StarRatingComponent
          name="hotelRating"
          editing={ false }
          value={ this.props.rating }
          renderStarIcon={ this.getRatingStyleIcon }
        />
        </HotelRatingStyling>
    );
  }
}

export default HotelRating;
