import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'react-grid-system';
import HotelImage from './HotelImage';
import HotelRating from './HotelRating';

const HotelCardStyling = styled.div`
  display: block;
  text-align: left;
  font-size: 16px;
  padding: 30px 0px;
  border-bottom-width: 1px;
  border-bottom-color: grey;
  border-bottom-style: solid;
`;

const HotelNameStyling = styled.div`
  font-size: 35px;
  font-weight: 500;
  max-width: 60%;
  display: inline-block;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

const HotelAddressStyling = styled.div`
  font-size: 16px;
  display: block;
  color: grey;
`;

const PriceStyling = styled.div`
  font-size: 35px;
`;

const RoomType = styled.div`
  color: red;
  text-decoration: underline;
`;

const DollarSign = styled.div`
  display: inline-block;
  vertical-align: top;
  font-size: 16px;
`;

const FreeCancellation = styled.div`
  color: green;
`;

const Saving = styled.div`
  color: red;
  font-size: 16px;
`;

const RightCol = styled.div`
  text-align: right;
`;

class HotelCard extends Component {
  static propTypes = {
    hotel: PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      address: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired,
      rating: PropTypes.number,
      ratingType: PropTypes.string,
      promotion: PropTypes.string,
      roomName: PropTypes.string,
      price: PropTypes.number.isRequired,
      savings: PropTypes.number,
      freeCancellation: PropTypes.bool.isRequired
    })
  };

  static defaultProps = {
    hotel: {
      id: '',
      title: '',
      address: '',
      image: '',
      rating: 0,
      ratingType: '',
      promotion: '',
      roomName: '',
      price: 0,
      savings: 0,
      freeCancellation: false
    }
  };

  isFreeCancellation = () => {
    return this.props.hotel.freeCancellation ? (
        <FreeCancellation>Free Cancellation</FreeCancellation>
    ) : (
        <div />
    );
  };

  hasSaving = () => {
    return this.props.hotel.savings > 0 ? (
        <Saving>Save ${this.props.hotel.savings}~</Saving>
    ) : (
        <div />
    );
  };

  renderRatingElement = () => {
    return this.props.hotel.rating && this.props.hotel.ratingType ? (
        <HotelRating
        rating={ this.props.hotel.rating }
        ratingType={ this.props.hotel.ratingType }
      />
    ) : null;
  };
  render() {
    return (
        <div>
            <HotelCardStyling>
                <Row>
                    <Col xs={ 2 }>
                        <HotelImage
                image={ this.props.hotel.image }
                badge={ this.props.hotel.promotion }
              />
                    </Col>
                    <Col xs={ 10 }>
                        <Row>
                            <Col>
                                <HotelNameStyling>{this.props.hotel.title}</HotelNameStyling>
                                {this.renderRatingElement()}
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <HotelAddressStyling>
                                    {this.props.hotel.address}
                                </HotelAddressStyling>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <RoomType>{this.props.hotel.roomName}</RoomType>
                            </Col>
                            <Col>
                                <RightCol>1 night total (AUD)</RightCol>
                            </Col>
                        </Row>
                        <Row>
                            <Col />
                            <Col>
                                <RightCol>
                                    <PriceStyling>
                                        <DollarSign>$</DollarSign>
                                        {this.props.hotel.price}
                                    </PriceStyling>
                                </RightCol>
                            </Col>
                        </Row>
                        <Row>
                            <Col>{this.isFreeCancellation()}</Col>
                            <Col>
                                <RightCol>{this.hasSaving()}</RightCol>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </HotelCardStyling>
        </div>
    );
  }
}

export default HotelCard;
