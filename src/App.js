import React, { Component } from 'react';
import { Container, Row, Col } from 'react-grid-system';
import styled from 'styled-components';
import { sortBy, size } from 'underscore';

import Logo from './components/Logo';
import ResultsLabel from './components/ResultsLabel';
import ResultsSort from './components/ResultsSort';
import HotelCard from './components/HotelCard';

import getHotelData from './utils/api';

const mockApiUrl = 'http://localhost:3001/hotels';

import './assets/App.css';

const AppWrapper = styled.div`
  color: black;
`;

const NoResultsStyling = styled.div`
  display: block;
  text-align: center;
  font-size: 30px;
  padding: 30px 0px;
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      resultCount: 0,
      hotels: {}
    };

    this.handlePriceSorting = this.handlePriceSorting.bind(this);
    this.sortHotelPriceAsc = this.sortHotelPriceAsc.bind(this);
    this.sortHotelPriceDesc = this.sortHotelPriceDesc.bind(this);
  }

  async loadHotels() {
    const hotelSearchResults = await getHotelData(mockApiUrl);

    this.setState({
      resultCount: hotelSearchResults.results,
      hotels: hotelSearchResults.hotelResults,
      loading: false
    });

    this.sortHotelPriceAsc();
  }

  createHotelCard = hotel => {
    return <HotelCard hotel={ hotel } />;
  };

  createListOfHotelCards = hotels => {
    if (this.state.resultCount > 0) {
      return hotels.map(item => this.createHotelCard(item));
    } else {
      return (
          <Row>
              <NoResultsStyling>Sorry, we didn't find any results</NoResultsStyling>
          </Row>
      );
    }
  };

  sortHotelPriceAsc = () => {
    const hotelsAsc = sortBy(this.state.hotels, 'price');
    this.setState({ hotels: hotelsAsc, sort: 'price-asc' });
  };

  sortHotelPriceDesc = () => {
    const hotelsDesc = sortBy(this.state.hotels, 'price').reverse();
    this.setState({ hotels: hotelsDesc, sort: 'price-desc' });
  };

  handlePriceSorting = direction => {
    this.setState({ sort: direction });
    if (direction == 'price-desc') {
      this.sortHotelPriceDesc();
    }
    if (direction == 'price-asc') {
      this.sortHotelPriceAsc();
    }
  };

  componentDidMount() {
    this.loadHotels();
  }

  render() {
    return this.state.loading ? (
        <div />
    ) : (
        <div>
            <AppWrapper>
                <Container>
                    <Row md={ 12 }>
                        <Col xs={ 3 }>
                            <Logo />
                        </Col>
                        <Col xs={ 9 } />
                    </Row>
                    <Row md={ 12 }>
                        <Col md={ 4 }>
                            <ResultsLabel
                  results={ this.state.resultCount }
                  suburb={ 'Sydney' }
                />
                        </Col>
                        <Col md={ 8 }>
                            <ResultsSort
                  value={ this.state.sort }
                  handlePriceSorting={ this.handlePriceSorting }
                />
                        </Col>
                    </Row>
                    {this.createListOfHotelCards(this.state.hotels)}
                </Container>
            </AppWrapper>
        </div>
    );
  }
}

export default App;
