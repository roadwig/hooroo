import { size } from 'underscore';

const getHotelData = async hotelApiUrl => {
  try {
    const response = await fetch(hotelApiUrl);
    const hotelResults = await response.json();
    const numberOfHotels = size(hotelResults);
    if (numberOfHotels > 0) {
      return { results: numberOfHotels, hotelResults };
    } else {
      return { results: numberOfHotels };
    }
  } catch (e) {
    throw new Error(`Uh oh - Failed with the following error: ${ e }`);
  }
};

export default getHotelData;
